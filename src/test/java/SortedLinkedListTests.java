import cz.mejty.SortedLinkedList;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

public class SortedLinkedListTests {
    @Nested
    public class AddTests {
        @Test
        public void testEmptyList() {
            var list = new SortedLinkedList<Integer>();
            list.add(1);
            assertEquals(1, list.size());
        }


        @Test
        public void testStartOfList() {
            var list = new SortedLinkedList<Integer>();
            list.add(3);

            list.add(1);

            assertEquals(1, list.stream().findFirst().get());
            assertEquals(3, list.stream().skip(1).findFirst().get());
        }

        @Test
        public void testEndOfList() {
            var list = new SortedLinkedList<Integer>();
            list.add(1);

            list.add(3);

            assertEquals(1, list.stream().findFirst().get());
            assertEquals(3, list.stream().skip(1).findFirst().get());
        }

        @Test
        public void testMultipleValuesList() {
            var list = new SortedLinkedList<Integer>();

            list.add(4);
            list.add(5);
            list.add(1);
            list.add(2);
            list.add(3);

            assertEquals(List.of(1, 2, 3, 4, 5), list.stream().toList());
        }

        @Test
        public void testSameOnStartOfList() {
            var list = new SortedLinkedList<Integer>();
            list.add(3);

            list.add(1);
            list.add(1);

            assertEquals(List.of(1, 1, 3), list.stream().toList());

        }

        @Test
        public void testMultipleStringValuesList() {
            var list = new SortedLinkedList<String>();

            list.add("b");
            list.add("a");
            list.add("ccc");
            list.add("d");
            list.add("f");

            assertEquals(List.of("a", "b", "ccc", "d", "f"), list.stream().toList());
        }
    }


    @Nested
    public class SizeTests {
        @Test
        public void testEmptyList() {
            var list = new SortedLinkedList<Integer>();

            assertEquals(0, list.size());
        }


        @Test
        public void testAfterAdd() {
            var list = new SortedLinkedList<Integer>();
            var sourceList = List.of(4, 3, 3);

            list.addAll(sourceList);

            assertEquals(sourceList.size(), list.size());
        }

        @Test
        public void testAfterRemove() {
            var list = new SortedLinkedList<Integer>();
            list.add(5);
            list.add(7);
            list.add(1);
            list.add(3);

            assertEquals(4, list.size());

            list.remove(3);

            assertEquals(3, list.size());
        }

        @Test
        public void testAfterClearList() {
            var list = new SortedLinkedList<Integer>();

            list.add(4);
            list.add(5);
            list.add(1);
            list.add(2);
            list.add(3);

            assertEquals(5, list.size());

            list.clear();

            assertEquals(0, list.size());
        }
    }

    @Nested
    public class IteratorTests {
        @Test
        public void testEmptyList() {
            var list = new SortedLinkedList<Integer>();
            var iterator = list.iterator();

            assertFalse(iterator.hasNext());
            assertThrows(NoSuchElementException.class, iterator::next);
        }


        @Test
        public void test2ElementList() {
            var list = new SortedLinkedList<Integer>();
            list.add(2);
            list.add(1);
            var iterator = list.iterator();

            assertTrue(iterator.hasNext());
            assertEquals(1, iterator.next());

            assertTrue(iterator.hasNext());
            assertEquals(2, iterator.next());

            assertFalse(iterator.hasNext());
        }
    }

    @Nested
    public class IteratorRemoveTests {
        @Test
        public void testEmptyList() {
            var list = new SortedLinkedList<Integer>();
            var iterator = list.iterator();

            assertThrows(IllegalStateException.class, iterator::remove);
        }


        @Test
        public void testItemRemoved() {
            var list = new SortedLinkedList<Integer>();
            list.add(2);
            list.add(1);
            list.add(3);
            list.add(4);

            var iterator = list.iterator();

            iterator.next();
            iterator.next();
            iterator.remove();

            assertEquals(List.of(1, 3, 4), list.stream().toList());
        }

        @Test
        public void testRemoveAllElements() {
            var list = new SortedLinkedList<Integer>();
            list.add(2);
            list.add(1);
            list.add(3);

            var iterator = list.iterator();

            iterator.next();
            assertDoesNotThrow(iterator::remove);
            iterator.next();
            assertDoesNotThrow(iterator::remove);
            iterator.next();
            assertDoesNotThrow(iterator::remove);
            assertTrue(list.isEmpty());
        }

        @Test
        public void testRemoveFirstElements() {
            var list = new SortedLinkedList<Integer>();
            list.add(2);
            list.add(1);
            list.add(3);

            var iterator = list.iterator();

            iterator.next();

            assertDoesNotThrow(iterator::remove);
            assertEquals(List.of(2, 3), list.stream().toList());
        }

        @Test
        public void testRemoveLastElements() {
            var list = new SortedLinkedList<Integer>();
            list.add(2);
            list.add(1);
            list.add(3);

            var iterator = list.iterator();

            iterator.next();
            iterator.next();
            iterator.next();

            assertDoesNotThrow(iterator::remove);
            assertEquals(List.of(1, 2), list.stream().toList());
        }

        @Test
        public void testRemoveSecondTimesThrows() {
            var list = new SortedLinkedList<Integer>();
            list.add(2);
            list.add(1);
            list.add(3);

            var iterator = list.iterator();
            iterator.next();

            assertDoesNotThrow(iterator::remove);
            assertThrows(IllegalStateException.class, iterator::remove);
            assertEquals(List.of(2, 3), list.stream().toList());
        }
    }


}
