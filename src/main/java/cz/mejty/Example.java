package cz.mejty;

import java.util.List;

public class Example {
    public static void main(String[] args) {
        System.out.println("Creating SortedLinkedList accepting integers");
        System.out.println("var integerList = new SortedLinkedList<Integer>()");
        var integerList = new SortedLinkedList<Integer>();
        System.out.println(integerList);

        System.out.println("Adding first item to list");
        System.out.println("integerList.add(2);");
        integerList.add(2);
        System.out.println(integerList);

        System.out.println("Adding two more items to list");
        System.out.println("integerList.add(1);");
        integerList.add(1);
        System.out.println(integerList);
        System.out.println("integerList.add(5);");
        integerList.add(5);
        System.out.println(integerList);

        System.out.println("Adding list if integers to list");
        System.out.println("integerList.addAll(List.of(1, 7, 8, 11, 9867, 6543));");
        integerList.addAll(List.of(1, 7, 8, 11, 9867, 6543));
        System.out.println(integerList);

        System.out.println();
        System.out.println("Creating SortedLinkedList accepting strings");
        System.out.println("var stringList = new SortedLinkedList<String>()");
        var stringList = new SortedLinkedList<String>();
        System.out.println(stringList);

        System.out.println("Adding two items to string list");
        System.out.println("stringList.add(\"Cecile\");");
        stringList.add("Cecile");
        System.out.println(stringList);
        System.out.println("stringList.add(\"Ananas\");");
        stringList.add("Ananas");
        System.out.println(stringList);
    }
}