package cz.mejty;

import java.util.*;

/*
    I decided not to implement List<E> interface because it requires to
    implement multiple index based operations e.g. add(int index, E element),
    that make no sense in context of List that keeps values sorted. For that reason
    I stayed with just Collection<T> interface
* */
public class SortedLinkedList<T extends Comparable<T>> extends AbstractCollection<T> {

    private Node<T> head;
    private int size;

    public SortedLinkedList() {
        this.head = null;
        this.size = 0;
    }

    @Override
    public Iterator<T> iterator() {
        return new ListIterator();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean add(final T item) {
        Objects.requireNonNull(item, "item must not be null");

        var newNode = new Node<>(item);
        ++size;

        if (head == null) {
            head = newNode;

            return true;
        }

        if (head.compareTo(newNode) > 0) {
            newNode.next = head;
            head = newNode;
            return true;
        }

        Node<T> prev = null;
        var curr = head;
        while (curr != null && curr.compareTo(newNode) <= 0) {
            prev = curr;
            curr = curr.getNext();
        }
        newNode.setNext(curr);
        prev.setNext(newNode);
        return true;
    }

    @Override
    public void clear() {
        head = null;
        size = 0;
    }

    private void unlink(Node<T> node, Node<T> prev) {
        if (prev == null) {
            head = node.getNext();
        } else {
            prev.setNext(node.getNext());
        }
        --size;
    }


    private class ListIterator implements Iterator<T> {
        private Node<T> lastReturnedPrev;
        private Node<T> lastReturned;
        private Node<T> next;
        private int nextIndex;

        ListIterator() {
            next = head;
            nextIndex = 0;
        }

        public boolean hasNext() {
            return nextIndex < size;
        }

        public T next() {
            if (!hasNext())
                throw new NoSuchElementException();

            lastReturnedPrev = Optional.ofNullable(lastReturned).orElse(lastReturnedPrev);
            lastReturned = next;
            next = next.getNext();
            nextIndex++;
            return lastReturned.getData();
        }


        public void remove() {
            if (lastReturned == null)
                throw new IllegalStateException();

            var lastNext = lastReturned.next;
            unlink(lastReturned, lastReturnedPrev);
            if (next == lastReturned)
                next = lastNext;
            else
                nextIndex--;
            lastReturned = null;
        }
    }


    private static class Node<A extends Comparable<A>> implements Comparable<Node<A>> {

        private final A data;
        private Node<A> next;

        Node(A data, Node<A> next) {
            this.data = data;
            this.next = next;
        }

        Node(A data) {
            this(data, null);
        }

        public A getData() {
            return data;
        }

        public Node<A> getNext() {
            return next;
        }

        public void setNext(Node<A> next) {
            this.next = next;
        }

        @Override
        public int compareTo(Node<A> other) {
            return data.compareTo(other.data);
        }
    }

}

