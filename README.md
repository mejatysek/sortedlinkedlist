# SorterLinkedList Project

This is simple project containing Java implementation of SorterLinkedList.

It was required that List works with `Integers` and `Strings` but thanks to generic it will
work with any object that implements `Comparable` interface.

## How to use it

```java
import cz.mejty.SortedLinkedList;

var integerList = new SortedLinkedList<Integer>(); // for integers version

""" or """

var stringList = new SortedLinkedList<String>(); // for strings version
```

More examples cloud be found in [Example.java](src/main/java/cz/mejty/Example.java) or in
[tests](src/test/java/SortedLinkedListTests.java)

## Run example

You could run example from your IDE or using maven:

```shell
 mvn exec:java -Dexec.mainClass="cz.mejty.Example"
```

## Run test

Test could be run using maven:

```shell
mvn test
```


## Known limitations

List currently does not accept `null` values and supports only ascending sort direction.